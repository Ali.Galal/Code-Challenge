var LocalStorageService = {
    insert : function (filename, item) {
        var list = [];
        list = JSON.parse(localStorage[filename]);
        if (typeof list == "undefined" || list == null) {
            list = [];
        }
        if (list.findIndex(x => x.username == item.username) != -1) {
            alert("this user already exist");
            return;
        }
        list.push(item);
        localStorage[filename] = JSON.stringify(list);
    }, update : function (filename, item, id) {
        var list = [];
        if (localStorage[filename]) {
            list = JSON.parse(localStorage[filename]);
            var idx = list.findIndex(x => x.id == id);
            if (idx != -1) {
                if (list.findIndex(x => x.username == item.username) != -1) {
                    alert("this user already exist")
                }
                list[idx] = item;
                localStorage[filename] = JSON.stringify(list);
            }
        }
    }, Delete : function (filename, id) {
        var list = this.select(filename);
        var idx = list.findIndex(x => x.id == id);
        if (idx != -1) {
            list.splice(idx, 1);
            if (list.length == 0) {
                localStorage.removeItem(filename);
            }
            else {
                localStorage[filename] = JSON.stringify(list);
            }
        }
        return list;
    }, select : function (filename) {
        var list = [];
        if (localStorage[filename]) {
            list = JSON.parse(localStorage[filename]);
            return list
        }
    }
}