var usersData;
var isEdit = false;
var editId;
function init() {
    LoadData();
    if (usersData != undefined)
        return;
    usersData = [];
    var user = {};
    user["username"] = "Ali";
    user["email"] = "ali.galal9011@gmail.com";
    user["password"] = "123";
    user["city"] = "Cairo"
    user["mobile"] = "01001358568";
    user["address"] = "Cairo";
    user["id"] = "1";
    usersData.push(user);
    localStorage["Users"] = JSON.stringify(usersData);
    LoadData();
}
function Save() {
    var user = {};
    user["username"] = document.getElementById("username").value;
    user["email"] = document.getElementById("email").value;
    user["password"] = document.getElementById("password").value;
    user["city"] = document.getElementById("city").value;
    user["mobile"] = document.getElementById("mobile").value;
    user["address"] = document.getElementById("address").value;
    user["id"] = Math.floor(Math.random() * 20).toString();
    if (isEdit)
        LocalStorageService.Delete("Users", editId);
    isEdit = false;
    LocalStorageService.insert("Users", user);
    usersData.push(user);
    LoadData();
    Clear();
}

function LoadData() {
    usersData = LocalStorageService.select("Users");
    LoadGrid();
}

function LoadGrid() {
    var htmlContent = new Array(), line = -1;
    if (usersData != undefined && usersData != null) {
        for (var i = 0; i < usersData.length; i++) {
            htmlContent[++line] = '<tr>';
            htmlContent[++line] = '<td>' + usersData[i].id + '</td>';
            htmlContent[++line] = '<td>' + usersData[i].username + '</td>';
            htmlContent[++line] = '<td>' + usersData[i].email + '</td>';
            htmlContent[++line] = '<td>' + usersData[i].mobile + '</td>';
            htmlContent[++line] = '<td>' + usersData[i].city + '</td>';
            htmlContent[++line] = '<td>' + usersData[i].address + '</td>';
            htmlContent[++line] = '<td class="center"><a onclick="Edit(' + usersData[i].id + ')"><i class="fa fa-edit"></i></a>';
            htmlContent[++line] = '<a onclick="Delete(' + usersData[i].id + ')"><i class="fa fa-trash"></i></a></td>';
        }

        document.getElementById("tbody").innerHTML = htmlContent.join('');
    }
    else {
        document.getElementById("tbody").innerHTML = "";
    }
}
function Clear() {
    document.getElementById("username").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("city").value = "";
    document.getElementById("address").value = "";
    document.getElementById("mobile").value = "";
}
function Edit(id) {
    var idx = usersData.findIndex(x => x.id == id);
    document.getElementById("username").value = usersData[idx].username;
    document.getElementById("email").value = usersData[idx].email;
    document.getElementById("password").value = usersData[idx].password;
    document.getElementById("city").value = usersData[idx].city;
    document.getElementById("address").value = usersData[idx].address;
    document.getElementById("mobile").value = usersData[idx].mobile;
    usersData.splice(idx, 0);
    editId = id;
    isEdit = true;
}
function Delete(id) {
    var idx = usersData.findIndex(x => x.id == id);
    usersData.splice(idx, 1);
    LocalStorageService.Delete("Users", id);
    LoadData();
}
window.onload = function () {
    init();
}